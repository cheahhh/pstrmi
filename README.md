# Introduction

PSTRMI(Programmable System for Tracking and Recording Modifications to Inventory) is an inventory management system designed for restaurant owners to track their ingredient levels accurately. The system allows users to bundle ingredients together for each menu item, and thus subtracting a menu item will subtract its associated ingredients proportionately.

## Helpful Documents

[User Documentation(Manual)](https://docs.google.com/document/d/1R3ACnvb1ug-xLTqv6X8dx_eBnoyLmAeXt8-38YUoPxs/edit?usp=sharing)

[System Documentation](https://docs.google.com/document/d/1z-Mces6lrK4u4jd6BIbkzubbBESUCv8h0zGktQmu2co/edit?usp=sharing)

## Developers

[Ivan Cheah](mailto:ivancheahkf@gmail.com)

[Laura Price](mailto:LTLP1212@gmail.com)

[Joel Pobanz](mailto:joelpobanz@gmail.com)

## Third Party Credits

[IgnaceMaes - MaterialSkin.NET](https://github.com/IgnaceMaes/MaterialSkin)

[IcoJam - Blueberry Basic Icons](http://www.iconarchive.com/show/blueberry-basic-icons-by-icojam.html)

[Google - Noto Emoji Food Drink Icons](https://www.google.com/get/noto/help/emoji/)

## Real World Statement

This project has been developed as part of a classroom learning experience by students at Utah State University.  While efforts are made to ensure copyrights and intellectual property rights have not been violated, it is the responsibility of the organization using any classroom projects created by USU and its students to make sure the materials contained therein do not infringe the property rights (including without limitation rights of privacy and publicity, trademark rights, copyrights, patents, trade secrets, and licenses) of third parties.