﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_load = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_refresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngredientsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_adding = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_editing = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_removeing = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ts_flagged = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_addmenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_menuedit = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_menurem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_doc = New System.Windows.Forms.ToolStripMenuItem()
        Me.ts_about = New System.Windows.Forms.ToolStripMenuItem()
        Me.mts_main = New MaterialSkin.Controls.MaterialTabSelector()
        Me.mtc_main = New MaterialSkin.Controls.MaterialTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.dg_ingredient = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IngredientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Quantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cm_ingredient = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.EditToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaterialDivider1 = New MaterialSkin.Controls.MaterialDivider()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.t_search = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.btn_add_ingredient = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dg_menu = New System.Windows.Forms.DataGridView()
        Me.MenuID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cm_menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.edittoolstrip = New System.Windows.Forms.ToolStripMenuItem()
        Me.removetoolstrip = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaterialDivider2 = New MaterialSkin.Controls.MaterialDivider()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.t_search_menu = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.btn_menu_add = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.tmr_usertype_ing = New System.Windows.Forms.Timer(Me.components)
        Me.tmr_usertype_menu = New System.Windows.Forms.Timer(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.mtc_main.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dg_ingredient, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cm_ingredient.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dg_menu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cm_menu.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.IngredientsToolStripMenuItem, Me.MenuToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(638, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_load, Me.ts_refresh})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ts_load
        '
        Me.ts_load.Name = "ts_load"
        Me.ts_load.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.ts_load.Size = New System.Drawing.Size(210, 22)
        Me.ts_load.Text = "Load Spreadsheet"
        '
        'ts_refresh
        '
        Me.ts_refresh.Name = "ts_refresh"
        Me.ts_refresh.ShortcutKeys = System.Windows.Forms.Keys.F5
        Me.ts_refresh.Size = New System.Drawing.Size(210, 22)
        Me.ts_refresh.Text = "Refresh"
        '
        'IngredientsToolStripMenuItem
        '
        Me.IngredientsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_adding, Me.ts_editing, Me.ts_removeing, Me.ToolStripSeparator1, Me.ts_flagged})
        Me.IngredientsToolStripMenuItem.Name = "IngredientsToolStripMenuItem"
        Me.IngredientsToolStripMenuItem.Size = New System.Drawing.Size(78, 20)
        Me.IngredientsToolStripMenuItem.Text = "Ingredients"
        '
        'ts_adding
        '
        Me.ts_adding.Name = "ts_adding"
        Me.ts_adding.Size = New System.Drawing.Size(178, 22)
        Me.ts_adding.Text = "Add"
        '
        'ts_editing
        '
        Me.ts_editing.Name = "ts_editing"
        Me.ts_editing.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.ts_editing.Size = New System.Drawing.Size(178, 22)
        Me.ts_editing.Text = "Edit"
        '
        'ts_removeing
        '
        Me.ts_removeing.Name = "ts_removeing"
        Me.ts_removeing.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.ts_removeing.Size = New System.Drawing.Size(178, 22)
        Me.ts_removeing.Text = "Remove"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(175, 6)
        '
        'ts_flagged
        '
        Me.ts_flagged.CheckOnClick = True
        Me.ts_flagged.Name = "ts_flagged"
        Me.ts_flagged.Size = New System.Drawing.Size(178, 22)
        Me.ts_flagged.Text = "Flagged Ingredients"
        '
        'MenuToolStripMenuItem
        '
        Me.MenuToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_addmenu, Me.ts_menuedit, Me.ts_menurem})
        Me.MenuToolStripMenuItem.Name = "MenuToolStripMenuItem"
        Me.MenuToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.MenuToolStripMenuItem.Text = "Menu"
        '
        'ts_addmenu
        '
        Me.ts_addmenu.Name = "ts_addmenu"
        Me.ts_addmenu.Size = New System.Drawing.Size(141, 22)
        Me.ts_addmenu.Text = "Add"
        '
        'ts_menuedit
        '
        Me.ts_menuedit.Name = "ts_menuedit"
        Me.ts_menuedit.ShortcutKeys = System.Windows.Forms.Keys.F2
        Me.ts_menuedit.Size = New System.Drawing.Size(141, 22)
        Me.ts_menuedit.Text = "Edit"
        '
        'ts_menurem
        '
        Me.ts_menurem.Name = "ts_menurem"
        Me.ts_menurem.ShortcutKeys = System.Windows.Forms.Keys.Delete
        Me.ts_menurem.Size = New System.Drawing.Size(141, 22)
        Me.ts_menurem.Text = "Remove"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_doc, Me.ts_about})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'ts_doc
        '
        Me.ts_doc.Name = "ts_doc"
        Me.ts_doc.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.ts_doc.Size = New System.Drawing.Size(176, 22)
        Me.ts_doc.Text = "Documentation"
        '
        'ts_about
        '
        Me.ts_about.Name = "ts_about"
        Me.ts_about.Size = New System.Drawing.Size(176, 22)
        Me.ts_about.Text = "About"
        '
        'mts_main
        '
        Me.mts_main.BaseTabControl = Me.mtc_main
        Me.mts_main.Depth = 0
        Me.mts_main.Dock = System.Windows.Forms.DockStyle.Top
        Me.mts_main.Location = New System.Drawing.Point(0, 24)
        Me.mts_main.MouseState = MaterialSkin.MouseState.HOVER
        Me.mts_main.Name = "mts_main"
        Me.mts_main.Size = New System.Drawing.Size(638, 34)
        Me.mts_main.TabIndex = 1
        Me.mts_main.Text = "MaterialTabSelector1"
        '
        'mtc_main
        '
        Me.mtc_main.Controls.Add(Me.TabPage1)
        Me.mtc_main.Controls.Add(Me.TabPage2)
        Me.mtc_main.Depth = 0
        Me.mtc_main.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mtc_main.Location = New System.Drawing.Point(0, 58)
        Me.mtc_main.MouseState = MaterialSkin.MouseState.HOVER
        Me.mtc_main.Name = "mtc_main"
        Me.mtc_main.SelectedIndex = 0
        Me.mtc_main.Size = New System.Drawing.Size(638, 346)
        Me.mtc_main.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dg_ingredient)
        Me.TabPage1.Controls.Add(Me.MaterialDivider1)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(630, 320)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Ingredients"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'dg_ingredient
        '
        Me.dg_ingredient.AllowUserToAddRows = False
        Me.dg_ingredient.AllowUserToDeleteRows = False
        Me.dg_ingredient.BackgroundColor = System.Drawing.Color.White
        Me.dg_ingredient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ingredient.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.IngredientName, Me.Quantity, Me.Column1})
        Me.dg_ingredient.ContextMenuStrip = Me.cm_ingredient
        Me.dg_ingredient.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_ingredient.Location = New System.Drawing.Point(3, 38)
        Me.dg_ingredient.MultiSelect = False
        Me.dg_ingredient.Name = "dg_ingredient"
        Me.dg_ingredient.ReadOnly = True
        Me.dg_ingredient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ingredient.Size = New System.Drawing.Size(624, 279)
        Me.dg_ingredient.TabIndex = 2
        '
        'ID
        '
        Me.ID.DataPropertyName = "IngredientID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        '
        'IngredientName
        '
        Me.IngredientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IngredientName.DataPropertyName = "Name"
        Me.IngredientName.HeaderText = "Ingredient"
        Me.IngredientName.Name = "IngredientName"
        Me.IngredientName.ReadOnly = True
        '
        'Quantity
        '
        Me.Quantity.DataPropertyName = "Units"
        Me.Quantity.HeaderText = "Quantity"
        Me.Quantity.Name = "Quantity"
        Me.Quantity.ReadOnly = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "OrderTrigger"
        Me.Column1.HeaderText = "OrderTrigger"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'cm_ingredient
        '
        Me.cm_ingredient.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem1, Me.RemoveToolStripMenuItem1})
        Me.cm_ingredient.Name = "cm_ingredient"
        Me.cm_ingredient.Size = New System.Drawing.Size(118, 48)
        '
        'EditToolStripMenuItem1
        '
        Me.EditToolStripMenuItem1.Name = "EditToolStripMenuItem1"
        Me.EditToolStripMenuItem1.Size = New System.Drawing.Size(117, 22)
        Me.EditToolStripMenuItem1.Text = "Edit"
        '
        'RemoveToolStripMenuItem1
        '
        Me.RemoveToolStripMenuItem1.Name = "RemoveToolStripMenuItem1"
        Me.RemoveToolStripMenuItem1.Size = New System.Drawing.Size(117, 22)
        Me.RemoveToolStripMenuItem1.Text = "Remove"
        '
        'MaterialDivider1
        '
        Me.MaterialDivider1.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialDivider1.Depth = 0
        Me.MaterialDivider1.Dock = System.Windows.Forms.DockStyle.Top
        Me.MaterialDivider1.Location = New System.Drawing.Point(3, 36)
        Me.MaterialDivider1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialDivider1.Name = "MaterialDivider1"
        Me.MaterialDivider1.Size = New System.Drawing.Size(624, 2)
        Me.MaterialDivider1.TabIndex = 1
        Me.MaterialDivider1.Text = "MaterialDivider1"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.t_search)
        Me.Panel1.Controls.Add(Me.btn_add_ingredient)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(624, 33)
        Me.Panel1.TabIndex = 0
        '
        't_search
        '
        Me.t_search.Depth = 0
        Me.t_search.Hint = "Search"
        Me.t_search.Location = New System.Drawing.Point(5, 7)
        Me.t_search.MaxLength = 50
        Me.t_search.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_search.Name = "t_search"
        Me.t_search.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_search.SelectedText = ""
        Me.t_search.SelectionLength = 0
        Me.t_search.SelectionStart = 0
        Me.t_search.Size = New System.Drawing.Size(215, 23)
        Me.t_search.TabIndex = 0
        Me.t_search.TabStop = False
        Me.t_search.UseSystemPasswordChar = False
        '
        'btn_add_ingredient
        '
        Me.btn_add_ingredient.Depth = 0
        Me.btn_add_ingredient.Dock = System.Windows.Forms.DockStyle.Right
        Me.btn_add_ingredient.Location = New System.Drawing.Point(483, 0)
        Me.btn_add_ingredient.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_add_ingredient.Name = "btn_add_ingredient"
        Me.btn_add_ingredient.Primary = True
        Me.btn_add_ingredient.Size = New System.Drawing.Size(141, 33)
        Me.btn_add_ingredient.TabIndex = 1
        Me.btn_add_ingredient.Text = "Add Ingredient"
        Me.btn_add_ingredient.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.dg_menu)
        Me.TabPage2.Controls.Add(Me.MaterialDivider2)
        Me.TabPage2.Controls.Add(Me.Panel2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(630, 320)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Menu"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'dg_menu
        '
        Me.dg_menu.AllowUserToAddRows = False
        Me.dg_menu.AllowUserToDeleteRows = False
        Me.dg_menu.BackgroundColor = System.Drawing.Color.White
        Me.dg_menu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_menu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MenuID, Me.DataGridViewTextBoxColumn1})
        Me.dg_menu.ContextMenuStrip = Me.cm_menu
        Me.dg_menu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg_menu.Location = New System.Drawing.Point(3, 38)
        Me.dg_menu.MultiSelect = False
        Me.dg_menu.Name = "dg_menu"
        Me.dg_menu.ReadOnly = True
        Me.dg_menu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_menu.Size = New System.Drawing.Size(624, 279)
        Me.dg_menu.TabIndex = 4
        '
        'MenuID
        '
        Me.MenuID.DataPropertyName = "MenuID"
        Me.MenuID.HeaderText = "MenuID"
        Me.MenuID.Name = "MenuID"
        Me.MenuID.ReadOnly = True
        Me.MenuID.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Name"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Menu"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'cm_menu
        '
        Me.cm_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.edittoolstrip, Me.removetoolstrip})
        Me.cm_menu.Name = "cm_ingredient"
        Me.cm_menu.Size = New System.Drawing.Size(118, 48)
        '
        'edittoolstrip
        '
        Me.edittoolstrip.Name = "edittoolstrip"
        Me.edittoolstrip.Size = New System.Drawing.Size(117, 22)
        Me.edittoolstrip.Text = "Edit"
        '
        'removetoolstrip
        '
        Me.removetoolstrip.Name = "removetoolstrip"
        Me.removetoolstrip.Size = New System.Drawing.Size(117, 22)
        Me.removetoolstrip.Text = "Remove"
        '
        'MaterialDivider2
        '
        Me.MaterialDivider2.BackColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialDivider2.Depth = 0
        Me.MaterialDivider2.Dock = System.Windows.Forms.DockStyle.Top
        Me.MaterialDivider2.Location = New System.Drawing.Point(3, 36)
        Me.MaterialDivider2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialDivider2.Name = "MaterialDivider2"
        Me.MaterialDivider2.Size = New System.Drawing.Size(624, 2)
        Me.MaterialDivider2.TabIndex = 3
        Me.MaterialDivider2.Text = "MaterialDivider2"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.t_search_menu)
        Me.Panel2.Controls.Add(Me.btn_menu_add)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(624, 33)
        Me.Panel2.TabIndex = 2
        '
        't_search_menu
        '
        Me.t_search_menu.Depth = 0
        Me.t_search_menu.Hint = "Search"
        Me.t_search_menu.Location = New System.Drawing.Point(5, 7)
        Me.t_search_menu.MaxLength = 50
        Me.t_search_menu.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_search_menu.Name = "t_search_menu"
        Me.t_search_menu.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_search_menu.SelectedText = ""
        Me.t_search_menu.SelectionLength = 0
        Me.t_search_menu.SelectionStart = 0
        Me.t_search_menu.Size = New System.Drawing.Size(215, 23)
        Me.t_search_menu.TabIndex = 0
        Me.t_search_menu.TabStop = False
        Me.t_search_menu.UseSystemPasswordChar = False
        '
        'btn_menu_add
        '
        Me.btn_menu_add.Depth = 0
        Me.btn_menu_add.Dock = System.Windows.Forms.DockStyle.Right
        Me.btn_menu_add.Location = New System.Drawing.Point(483, 0)
        Me.btn_menu_add.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_menu_add.Name = "btn_menu_add"
        Me.btn_menu_add.Primary = True
        Me.btn_menu_add.Size = New System.Drawing.Size(141, 33)
        Me.btn_menu_add.TabIndex = 1
        Me.btn_menu_add.Text = "Add Menu Item"
        Me.btn_menu_add.UseVisualStyleBackColor = True
        '
        'tmr_usertype_ing
        '
        Me.tmr_usertype_ing.Interval = 300
        '
        'tmr_usertype_menu
        '
        Me.tmr_usertype_menu.Interval = 300
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 404)
        Me.Controls.Add(Me.mtc_main)
        Me.Controls.Add(Me.mts_main)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PSTRMI Inventory Management System"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.mtc_main.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dg_ingredient, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cm_ingredient.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dg_menu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cm_menu.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents mts_main As MaterialSkin.Controls.MaterialTabSelector
    Friend WithEvents mtc_main As MaterialSkin.Controls.MaterialTabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Panel1 As Panel
    Friend WithEvents t_search As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents btn_add_ingredient As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents dg_ingredient As DataGridView
    Friend WithEvents MaterialDivider1 As MaterialSkin.Controls.MaterialDivider
    Friend WithEvents MaterialDivider2 As MaterialSkin.Controls.MaterialDivider
    Friend WithEvents Panel2 As Panel
    Friend WithEvents t_search_menu As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents btn_menu_add As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents dg_menu As DataGridView
    Friend WithEvents ts_load As ToolStripMenuItem
    Friend WithEvents IngredientsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ts_adding As ToolStripMenuItem
    Friend WithEvents ts_editing As ToolStripMenuItem
    Friend WithEvents ts_removeing As ToolStripMenuItem
    Friend WithEvents cm_ingredient As ContextMenuStrip
    Friend WithEvents MenuToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ts_addmenu As ToolStripMenuItem
    Friend WithEvents ts_menuedit As ToolStripMenuItem
    Friend WithEvents ts_menurem As ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents RemoveToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ts_refresh As ToolStripMenuItem
    Friend WithEvents cm_menu As ContextMenuStrip
    Friend WithEvents edittoolstrip As ToolStripMenuItem
    Friend WithEvents removetoolstrip As ToolStripMenuItem
    Friend WithEvents MenuID As DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ts_doc As ToolStripMenuItem
    Friend WithEvents ts_about As ToolStripMenuItem
    Friend WithEvents ID As DataGridViewTextBoxColumn
    Friend WithEvents IngredientName As DataGridViewTextBoxColumn
    Friend WithEvents Quantity As DataGridViewTextBoxColumn
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents tmr_usertype_ing As Timer
    Friend WithEvents tmr_usertype_menu As Timer
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ts_flagged As ToolStripMenuItem
End Class
