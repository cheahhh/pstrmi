﻿Public Class AddMenuItem
    Public editmode As Boolean = False
    Dim itemexists As Boolean = False
    Public itemid As Integer
    Private Sub bt_inc_Click(sender As Object, e As EventArgs) Handles bt_inc.Click
        'checks if the ingredient exists
        If Not (GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name = " & Cnull(cb_ingredient.Text)).Rows(0).Item(0) = 0 Or String.IsNullOrWhiteSpace(t_units.Value) = True) Then
            If checkitemexists(cb_ingredient.Text) = False Then
                dg_ingredient.Rows.Add(New String() {cb_ingredient.Text, t_units.Value})
                t_units.Value = t_units.Minimum
            ElseIf bt_inc.Text = "SET" Then
                For i = 0 To dg_ingredient.Rows.Count - 1
                    If dg_ingredient.Rows(i).Cells(0).Value = cb_ingredient.Text Then dg_ingredient.Rows(i).Cells(1).Value = t_units.Value
                Next
            Else
                MsgBox("Ingredient is already associated with this menu item", MsgBoxStyle.OkOnly, "Error Code: 4")
            End If
        Else
            MsgBox("Ingredient Name is Invalid", MsgBoxStyle.OkOnly, "Error Code: 3")
        End If
        If checkitemexists(cb_ingredient.Text) = True Then
            bt_inc.Text = "SET"
        Else
            bt_inc.Text = "+"
        End If
    End Sub
    Private Function checkitemexists(ByVal itemname As String) As Boolean
        'this function checks if the input ingredient is already associated with the menu item.
        Dim exists As Boolean = False
        For i = 0 To dg_ingredient.Rows.Count - 1
            If dg_ingredient.Rows(i).Cells(0).Value = itemname Then
                exists = True
            End If
        Next
        Return exists
    End Function
    Private Sub AddMenuItem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'populates the ingredient dropdown from the ingredients table.
        cb_ingredient.DataSource = GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients]")
        cb_ingredient.DisplayMember = "Name"
    End Sub
    Private Sub TextBox_LostFocus(sender As Object, e As EventArgs) Handles t_name.LostFocus
        'checks if there are conflicts for the menu item name.
        If Not Disposing = True Then
            pb_exists.Visible = True
            If editmode = False Then
                If GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name = " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString = "0" Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The menu item name is available.")
                    itemexists = False
                    If CInt(GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString) > 0 Then
                        pb_exists.Image = My.Resources.attention_icon
                        Dim similaring As String
                        Dim dt As DataTable = GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True))
                        For i = 0 To dt.Rows.Count - 1
                            If similaring = "" Then similaring = dt.Rows(i).Item(0) & vbNewLine Else similaring += dt.Rows(i).Item(0) & vbNewLine
                        Next
                        ToolTip1.SetToolTip(pb_exists, "Similar menu item exists:" & vbNewLine & similaring)
                        itemexists = False
                    End If
                Else
                    pb_exists.Image = My.Resources.close_delete_icon
                    ToolTip1.SetToolTip(pb_exists, "The menu item name is NOT available.")
                    itemexists = True
                End If
            Else
                If GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name = " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString = "0" Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The menu item name is available.")
                    itemexists = False
                    'check if similarly named menu items exist, name identical to original does not conflict.
                    If CInt(GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True) & " AND Name <> " & Cnull(GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE MenuID = " & Cnull(itemid)).Rows(0).Item(0).ToString, True)).Rows(0).Item(0).ToString) > 0 Then
                        pb_exists.Image = My.Resources.attention_icon
                        Dim similaring As String
                        Dim dt As DataTable = GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True) & " AND Name <> " & Cnull(GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE MenuID = " & Cnull(itemid)).Rows(0).Item(0).ToString, True))
                        For i = 0 To dt.Rows.Count - 1
                            If similaring = "" Then similaring = dt.Rows(i).Item(0) & vbNewLine Else similaring += dt.Rows(i).Item(0) & vbNewLine
                        Next
                        ToolTip1.SetToolTip(pb_exists, "Similar menu item exists:" & vbNewLine & similaring)
                        itemexists = False
                    End If
                ElseIf GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE MenuID = " & Cnull(itemid)).Rows(0).Item(0).ToString = t_name.Text Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The menu item name is available.")
                    itemexists = False
                Else
                    pb_exists.Image = My.Resources.close_delete_icon
                    ToolTip1.SetToolTip(pb_exists, "The menu item name is NOT available.")
                    itemexists = True
                End If
            End If
        End If
    End Sub
    Private Sub TextBox_gotFocus(sender As Object, e As EventArgs) Handles t_name.GotFocus
        pb_exists.Visible = False
    End Sub

    Private Sub MaterialRaisedButton3_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        If Not (dg_ingredient.Rows.Count = 0 Or String.IsNullOrWhiteSpace(t_name.Text) = True Or itemexists = True) Then
            If editmode = False Then
                GetData("INSERT INTO [" & initcat & "].[dbo].[MenuItems] VALUES(" & cnull(t_name.Text) & ");")
                Dim menuitemid As Integer = GetData("SELECT MAX(MenuID) FROM [" & initcat & "].[dbo].[MenuItems]").Rows(0).Item(0)
                For i = 0 To dg_ingredient.Rows.Count - 1
                    GetData("INSERT INTO [" & initcat & "].[dbo].[BridgeTable] VALUES(" & cnull(menuitemid) & ", " & cnull(returningredientbyname(dg_ingredient.Rows(i).Cells(0).Value)) & ", " & cnull(dg_ingredient.Rows(i).Cells(1).Value) & ")")
                Next
            ElseIf editmode = True Then
                GetData("UPDATE [" & initcat & "].[dbo].[MenuItems] SET Name = " & cnull(t_name.Text) & " WHERE MenuID = " & cnull(itemid))
                GetData("DELETE FROM [" & initcat & "].[dbo].[BridgeTable] WHERE MenuID = " & cnull(itemid))
                For i = 0 To dg_ingredient.Rows.Count - 1
                    GetData("INSERT INTO [" & initcat & "].[dbo].[BridgeTable] VALUES(" & cnull(itemid) & ", " & cnull(returningredientbyname(dg_ingredient.Rows(i).Cells(0).Value)) & ", " & cnull(dg_ingredient.Rows(i).Cells(1).Value) & ")")
                Next
            End If
            Form1.Show()
            Form1.UpdateMenuItems()
            Me.Close()
        Else
            Dim errormessage As String
            If dg_ingredient.Rows.Count = 0 Then errormessage += "Menu item has no ingredients associated with it." & vbNewLine
            If String.IsNullOrWhiteSpace(t_name.Text) = True Then errormessage += "Menu item name cannot be blank." & vbNewLine
            If itemexists = True Then errormessage += "Menu item name already exists in the database."
            MsgBox(errormessage, MsgBoxStyle.OkOnly, "Error Code: 7")
        End If
    End Sub
    Public Sub Populateingredients()
        'add all associated ingredients to the table.
        t_name.Text = GetData("SELECT Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE MenuID = " & Cnull(itemid)).Rows(0).Item(0)
        Dim dt As DataTable = GetData("SELECT ig.Name, br.Quantity FROM [" & initcat & "].[dbo].[Ingredients] AS ig JOIN [" & initcat & "].[dbo].[BridgeTable] AS br ON ig.IngredientID = br.IngredientID WHERE br.MenuID = " & Cnull(itemid))
        For i = 0 To dt.Rows.Count - 1
            dg_ingredient.Rows.Add(New String() {dt.Rows(i).Item(0), dt.Rows(i).Item(1)})
        Next
    End Sub
    Private Sub cm_menuedit_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cm_menuedit.Opening
        If dg_ingredient.SelectedRows.Count = 0 Then
            e.Cancel = True
        End If
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteToolStripMenuItem.Click
        dg_ingredient.Rows.Remove(dg_ingredient.SelectedRows(0))
    End Sub

    Private Sub t_units_KeyPress(sender As Object, e As KeyPressEventArgs) Handles t_units.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            e.Handled = True
            bt_inc.PerformClick()
        End If
    End Sub

    Private Sub dg_ingredient_MouseDown(sender As Object, e As MouseEventArgs) Handles dg_ingredient.MouseDown
        'this function ensures that the ingredient is also selected on a right mouse click event.
        If e.Button = MouseButtons.Right Then
            Dim hti As DataGridView.HitTestInfo = dg_ingredient.HitTest(e.X, e.Y)
            dg_ingredient.ClearSelection()
            If Not hti.RowIndex = -1 Then
                dg_ingredient.Rows(hti.RowIndex).Selected = True
            End If
        End If
    End Sub

    Private Sub cb_ingredient_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_ingredient.SelectedIndexChanged
        If checkitemexists(cb_ingredient.Text) = True Then
            bt_inc.Text = "SET"
        Else
            bt_inc.Text = "+"
        End If
    End Sub
End Class