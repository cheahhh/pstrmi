﻿Imports System.Drawing
Public Class ColorScheme
    Public Enum Primary
        PastramiRed = &HE830F0E
        PastramiRed2 = &HEB01513
        PastramiRed3 = &HE570A09
    End Enum
    Public Enum Accent
        PastramiRedAccent2 = &HEEA6312
    End Enum
End Class
