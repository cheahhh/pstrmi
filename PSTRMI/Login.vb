﻿Imports MaterialSkin
Imports MaterialSkin.Controls
Public Class Login
    Dim materialskinmanager As MaterialSkinManager
    Private Sub cb_DropDown(sender As Object, e As EventArgs) Handles cb.DropDown
        Try
            username = t_user.Text
            password = t_pass.Text
            serverstring = t_ip.Text
            'populates the dropdown menu based on the databases the user has access to.
            cb.DataSource = GetData("DECLARE @DBuser_sql VARCHAR(4000) 
DECLARE @DBuser_table TABLE (DBName VARCHAR(200), UserName VARCHAR(250), LoginType VARCHAR(500), AssociatedRole VARCHAR(200)) 
SET @DBuser_sql='SELECT ''?'' AS DBName,a.name AS Name,a.type_desc AS LoginType,USER_NAME(b.role_principal_id) AS AssociatedRole FROM ?.sys.database_principals a 
LEFT OUTER JOIN ?.sys.database_role_members b ON a.principal_id=b.member_principal_id 
WHERE a.sid NOT IN (0x01,0x00) AND a.sid IS NOT NULL AND a.type NOT IN (''C'') AND a.is_fixed_role <> 1 AND a.name NOT LIKE ''##%'' AND ''?'' NOT IN (''master'',''msdb'',''model'',''tempdb'') ORDER BY Name'
INSERT @DBuser_table 
EXEC sp_MSforeachdb @command1=@dbuser_sql 
SELECT [DBName] FROM @DBuser_table WHERE [AssociatedRole] = 'db_owner' AND [UserName] = " & Cnull(t_user.Text) & " ORDER BY DBName", False)
            cb.DisplayMember = "DBName"
        Catch ex As Exception
            MsgBox("Login failed, please double check credentials", MsgBoxStyle.OkOnly, "Error Code: 0")
        End Try
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        Try
            username = t_user.Text
            password = t_pass.Text
            serverstring = t_ip.Text
            initcat = cb.Text
            'verify if credentials are valid.
            GetData("DECLARE @DBuser_sql VARCHAR(4000) 
DECLARE @DBuser_table TABLE (DBName VARCHAR(200), UserName VARCHAR(250), LoginType VARCHAR(500), AssociatedRole VARCHAR(200)) 
SET @DBuser_sql='SELECT ''?'' AS DBName,a.name AS Name,a.type_desc AS LoginType,USER_NAME(b.role_principal_id) AS AssociatedRole FROM ?.sys.database_principals a 
LEFT OUTER JOIN ?.sys.database_role_members b ON a.principal_id=b.member_principal_id 
WHERE a.sid NOT IN (0x01,0x00) AND a.sid IS NOT NULL AND a.type NOT IN (''C'') AND a.is_fixed_role <> 1 AND a.name NOT LIKE ''##%'' AND ''?'' NOT IN (''master'',''msdb'',''model'',''tempdb'') ORDER BY Name'
INSERT @DBuser_table 
EXEC sp_MSforeachdb @command1=@dbuser_sql 
SELECT [DBName] FROM @DBuser_table WHERE [AssociatedRole] = 'db_owner' AND [UserName] = " & Cnull(t_user.Text) & " ORDER BY DBName", False)
            If cb_remember.Checked = True Then
                My.Settings.ip = serverstring
                My.Settings.user = username
                My.Settings.password = password
                My.Settings.db = initcat
                My.Settings.Save()
            Else
                My.Settings.Reset()
                My.Settings.Save()
            End If
            Me.Hide()
            Form1.Show()
        Catch ex As Exception
            MsgBox("Login failed, please double check credentials", MsgBoxStyle.OkOnly, "Error Code: 0")
        End Try
    End Sub

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Uncomment line below to unlock Secret PSTRMI Color Scheme
        If My.Settings.usetheme = True Then
            materialskinmanager = MaterialSkinManager.Instance
            materialskinmanager.Theme = MaterialSkinManager.Themes.LIGHT
            materialskinmanager.ColorScheme = New MaterialSkin.ColorScheme(ColorScheme.Primary.PastramiRed, ColorScheme.Primary.PastramiRed2, ColorScheme.Primary.PastramiRed3, ColorScheme.Accent.PastramiRedAccent2, TextShade.WHITE)
        End If
        If String.IsNullOrEmpty(My.Settings.ip) = False Then
            Try
                'load credentials from saved values
                t_ip.Text = My.Settings.ip
                t_user.Text = My.Settings.user
                t_pass.Text = My.Settings.password
                username = t_user.Text
                password = t_pass.Text
                serverstring = t_ip.Text
                cb.DataSource = GetData("DECLARE @DBuser_sql VARCHAR(4000) 
DECLARE @DBuser_table TABLE (DBName VARCHAR(200), UserName VARCHAR(250), LoginType VARCHAR(500), AssociatedRole VARCHAR(200)) 
SET @DBuser_sql='SELECT ''?'' AS DBName,a.name AS Name,a.type_desc AS LoginType,USER_NAME(b.role_principal_id) AS AssociatedRole FROM ?.sys.database_principals a 
LEFT OUTER JOIN ?.sys.database_role_members b ON a.principal_id=b.member_principal_id 
WHERE a.sid NOT IN (0x01,0x00) AND a.sid IS NOT NULL AND a.type NOT IN (''C'') AND a.is_fixed_role <> 1 AND a.name NOT LIKE ''##%'' AND ''?'' NOT IN (''master'',''msdb'',''model'',''tempdb'') ORDER BY Name'
INSERT @DBuser_table 
EXEC sp_MSforeachdb @command1=@dbuser_sql 
SELECT [DBName] FROM @DBuser_table WHERE [AssociatedRole] = 'db_owner' AND [UserName] = " & Cnull(t_user.Text) & " ORDER BY DBName", False)
                cb.DisplayMember = "DBName"
                cb.SelectedText = My.Settings.db
                initcat = cb.SelectedText
                cb_remember.Checked = True
            Catch ex As Exception
                MsgBox("Login failed, please double check credentials", MsgBoxStyle.OkOnly, "Error Code: 0")
            End Try
        End If
    End Sub
End Class