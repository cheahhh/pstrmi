﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_about
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_about))
        Me.t_realworld = New System.Windows.Forms.Label()
        Me.t_logo = New System.Windows.Forms.Label()
        Me.t_desc = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        't_realworld
        '
        Me.t_realworld.AutoSize = True
        Me.t_realworld.BackColor = System.Drawing.Color.Transparent
        Me.t_realworld.ForeColor = System.Drawing.Color.White
        Me.t_realworld.Location = New System.Drawing.Point(12, 100)
        Me.t_realworld.Name = "t_realworld"
        Me.t_realworld.Size = New System.Drawing.Size(349, 156)
        Me.t_realworld.TabIndex = 2
        Me.t_realworld.Text = resources.GetString("t_realworld.Text")
        Me.ToolTip1.SetToolTip(Me.t_realworld, "Click to close.")
        '
        't_logo
        '
        Me.t_logo.AutoSize = True
        Me.t_logo.BackColor = System.Drawing.Color.Transparent
        Me.t_logo.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.t_logo.ForeColor = System.Drawing.Color.White
        Me.t_logo.Location = New System.Drawing.Point(54, 9)
        Me.t_logo.Name = "t_logo"
        Me.t_logo.Size = New System.Drawing.Size(270, 55)
        Me.t_logo.TabIndex = 3
        Me.t_logo.Text = "P.S.T.R.M.I"
        Me.ToolTip1.SetToolTip(Me.t_logo, "Click to close.")
        '
        't_desc
        '
        Me.t_desc.AutoSize = True
        Me.t_desc.BackColor = System.Drawing.Color.Transparent
        Me.t_desc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.t_desc.ForeColor = System.Drawing.Color.White
        Me.t_desc.Location = New System.Drawing.Point(43, 64)
        Me.t_desc.Name = "t_desc"
        Me.t_desc.Size = New System.Drawing.Size(294, 26)
        Me.t_desc.TabIndex = 4
        Me.t_desc.Text = "PROGRAMMABLE SYSTEM FOR TRACKING AND " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "RECORDING MODIFICATIONS TO INVENTORY"
        Me.ToolTip1.SetToolTip(Me.t_desc, "Click to close.")
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel1.Location = New System.Drawing.Point(15, 19)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(10, 71)
        Me.Panel1.TabIndex = 5
        Me.Panel1.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel2.Location = New System.Drawing.Point(314, 19)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(10, 71)
        Me.Panel2.TabIndex = 6
        Me.Panel2.Visible = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel3.Location = New System.Drawing.Point(330, 39)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(10, 51)
        Me.Panel3.TabIndex = 7
        Me.Panel3.Visible = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel4.Location = New System.Drawing.Point(15, 182)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(10, 71)
        Me.Panel4.TabIndex = 8
        Me.Panel4.Visible = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Gray
        Me.Panel5.Location = New System.Drawing.Point(31, 182)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(10, 71)
        Me.Panel5.TabIndex = 6
        Me.Panel5.Visible = False
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.DodgerBlue
        Me.Panel6.Location = New System.Drawing.Point(267, 185)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(10, 71)
        Me.Panel6.TabIndex = 9
        Me.Panel6.Visible = False
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Panel7.Location = New System.Drawing.Point(283, 226)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(78, 12)
        Me.Panel7.TabIndex = 10
        Me.Panel7.Visible = False
        '
        'frm_about
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = Global.PSTRMI.My.Resources.Resources.pstrmi_bg
        Me.ClientSize = New System.Drawing.Size(377, 265)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.t_desc)
        Me.Controls.Add(Me.t_logo)
        Me.Controls.Add(Me.t_realworld)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.KeyPreview = True
        Me.Name = "frm_about"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About PSTRMI"
        Me.ToolTip1.SetToolTip(Me, "Click to close.")
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents t_realworld As Label
    Friend WithEvents t_logo As Label
    Friend WithEvents t_desc As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents ToolTip1 As ToolTip
End Class
