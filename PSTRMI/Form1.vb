﻿Imports Microsoft.VisualBasic.FileIO.TextFieldParser
Public Class Form1
#Region "Ingredients"
    Private Sub dg_ingredient_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles dg_ingredient.RowPostPaint
        'checks if the ingredient falls below the minimum threshold and paints it red if it does.
        If (dg_ingredient.Rows(e.RowIndex).Cells(2).Value <= dg_ingredient.Rows(e.RowIndex).Cells(3).Value) Then
            dg_ingredient.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.Red
        End If
    End Sub
    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles btn_add_ingredient.Click
        ts_adding.PerformClick()
    End Sub
    Public Sub Updateingredients()
        'loads ingredients from database onto table.
        Dim querystring As String = "SELECT IngredientID, Name, Units, OrderTrigger FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name LIKE " & Cnull("%" & t_search.Text & "%", True)
        If ts_flagged.Checked = True Then querystring += " AND Units <= OrderTrigger"
        dg_ingredient.DataSource = GetData(querystring)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Updateingredients()
    End Sub

    Private Sub dg_ingredient_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dg_ingredient.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            ts_editing.PerformClick()
        End If
    End Sub
    Private Sub IngredientsToolStripMenuItem_DropDownOpening(sender As Object, e As EventArgs) Handles IngredientsToolStripMenuItem.DropDownOpening
        'gray out disabled options for error prevention.
        If mtc_main.SelectedIndex = 0 Then
            If dg_ingredient.SelectedRows.Count = 0 Then
                ts_editing.Enabled = False
                ts_removeing.Enabled = False
            Else
                ts_editing.Enabled = True
                ts_removeing.Enabled = True
            End If
        Else
            ts_editing.Enabled = False
            ts_removeing.Enabled = False
        End If
    End Sub

    Private Sub ts_adding_Click(sender As Object, e As EventArgs) Handles ts_adding.Click
        Dim frm As New AddNewIngredient
        frm.ShowDialog()
    End Sub

    Private Sub cm_ingredient_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cm_ingredient.Opening
        'disable right click menu if there are no selected ingredients.
        If dg_ingredient.SelectedRows.Count = 0 Then
            e.Cancel = True
        Else
            ts_editing.Enabled = True
            ts_removeing.Enabled = True
        End If
    End Sub

    Private Sub EditToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles EditToolStripMenuItem1.Click
        ts_editing.PerformClick()
    End Sub

    Private Sub RemoveToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RemoveToolStripMenuItem1.Click
        ts_removeing.PerformClick()
    End Sub

    Private Sub ts_editing_Click(sender As Object, e As EventArgs) Handles ts_editing.Click
        'Modify and display the addnewingredient window so it is in edit mode.
        Dim frm As New AddNewIngredient
        frm.Text = "Edit Ingredient"
        frm.editmode = True
        frm.itemid = dg_ingredient.SelectedRows(0).Cells(0).Value
        frm.Populatefields()
        frm.ShowDialog()
    End Sub

    Private Sub ts_removeing_Click(sender As Object, e As EventArgs) Handles ts_removeing.Click
        'Prompt user for confirmation to delete the ingredient item and remove all relationships in the bridge table.
        If MsgBox("Are you sure you want to delete " & dg_ingredient.SelectedRows(0).Cells(1).Value & "?", MsgBoxStyle.YesNo, "Confirm Deletion?") = MsgBoxResult.Yes Then
            GetData("DELETE FROM [" & initcat & "].[dbo].[BridgeTable] WHERE IngredientID = " & Cnull(dg_ingredient.SelectedRows(0).Cells(0).Value))
            GetData("DELETE FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(dg_ingredient.SelectedRows(0).Cells(0).Value))
            Updateingredients()
        End If
    End Sub
#End Region
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        'Prompt user for confirmation to log out.
        e.Cancel = True
        If MsgBox("Are you sure you want to log out?", MsgBoxStyle.YesNo, "Log out?") = MsgBoxResult.Yes Then
            End
        End If
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ts_refresh.Click
        'Refreshes the view depending on what tab the system is on.
        If mtc_main.SelectedIndex = 0 Then
            Updateingredients()
        Else
            UpdateMenuItems()
        End If
    End Sub
#Region "Menu Items"
    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs) Handles btn_menu_add.Click
        ts_addmenu.PerformClick()
    End Sub
    Public Sub UpdateMenuItems()
        'Display the menu items.
        dg_menu.DataSource = GetData("SELECT MenuID, Name FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name LIKE " & Cnull("%" & t_search_menu.Text & "%", True))
    End Sub

    Private Sub dg_menu_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dg_menu.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            ts_menuedit.PerformClick()
        End If
    End Sub
    Private Sub cm_menu_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles cm_menu.Opening
        'disable contextmenustrip(right click menu) if there are no menu items.
        If dg_menu.SelectedRows.Count = 0 Then
            e.Cancel = True
        Else
            ts_menuedit.Enabled = True
            ts_menurem.Enabled = True
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles edittoolstrip.Click
        ts_menuedit.PerformClick()
    End Sub

    Private Sub MenuToolStripMenuItem_DropDownOpening(sender As Object, e As EventArgs) Handles MenuToolStripMenuItem.DropDownOpening
        'Gray out disabled options for error prevention
        If mtc_main.SelectedIndex = 1 Then
            If dg_menu.SelectedRows.Count = 0 Then
                ts_menuedit.Enabled = False
                ts_menurem.Enabled = False
            Else
                ts_menuedit.Enabled = True
                ts_menurem.Enabled = True
            End If
        Else
            ts_menuedit.Enabled = False
            ts_menurem.Enabled = False
        End If
    End Sub

    Private Sub AddToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ts_addmenu.Click
        Dim frm As New AddMenuItem
        frm.ShowDialog()
    End Sub

    Private Sub ts_menuedit_Click(sender As Object, e As EventArgs) Handles ts_menuedit.Click
        'Modify and display the addmenuitem window so that it is in edit mode.
        Dim frm As New AddMenuItem
        frm.Text = "Edit Menu Item"
        frm.editmode = True
        frm.itemid = dg_menu.SelectedRows(0).Cells(0).Value
        frm.Populateingredients()
        frm.ShowDialog()
    End Sub

    Private Sub ts_menurem_Click(sender As Object, e As EventArgs) Handles ts_menurem.Click
        'Prompt user for confirmation to delete the menu item and clear the relationships with ingredients in the bridge table.
        If MsgBox("Are you sure you want to delete " & dg_menu.SelectedRows(0).Cells(1).Value & "?", MsgBoxStyle.YesNo, "Confirm Deletion?") = MsgBoxResult.Yes Then
            GetData("DELETE FROM [" & initcat & "].[dbo].[BridgeTable] WHERE MenuID = " & Cnull(dg_menu.SelectedRows(0).Cells(0).Value))
            GetData("DELETE FROM [" & initcat & "].[dbo].[MenuItems] WHERE MenuID = " & Cnull(dg_menu.SelectedRows(0).Cells(0).Value))
            UpdateMenuItems()
        End If
    End Sub

    Private Sub removetoolstrip_Click(sender As Object, e As EventArgs) Handles removetoolstrip.Click
        ts_menurem.PerformClick()
    End Sub
#End Region
    Private Sub mtc_main_SelectedIndexChanged(sender As Object, e As EventArgs) Handles mtc_main.SelectedIndexChanged
        'Refreshes Ingredients or Menu items based on which tab the system is on.
        'Disables irrelevant menu options so the keyboard shortcuts corresponds to which tab the user is on.
        If mtc_main.SelectedIndex = 0 Then
            Updateingredients()
            ts_editing.Enabled = True
            ts_removeing.Enabled = True
            ts_flagged.Enabled = True
            ts_menuedit.Enabled = False
            ts_menurem.Enabled = False
        Else
            UpdateMenuItems()
            ts_editing.Enabled = False
            ts_removeing.Enabled = False
            ts_flagged.Enabled = False
            ts_menuedit.Enabled = True
            ts_menurem.Enabled = True
        End If
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ts_about.Click
        'shows the about window.
        frm_about.Show()
    End Sub

    Private Sub DocumentationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ts_doc.Click
        'Start a web browser that navigates to the user documentation.
        Process.Start("https://docs.google.com/document/d/1R3ACnvb1ug-xLTqv6X8dx_eBnoyLmAeXt8-38YUoPxs/edit?usp=sharing")
    End Sub
#Region "Parse CSV"
    Private Sub LoadSpreadsheetToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ts_load.Click
        'This function parses the selected CSV file.
        Dim dlg As New OpenFileDialog
        dlg.Title = "Select CSV file"
        dlg.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*"
        If dlg.ShowDialog = DialogResult.OK Then
            Dim afile As FileIO.TextFieldParser = New FileIO.TextFieldParser(dlg.FileName)
            Dim currentrecord As String()
            afile.Delimiters = New String() {","}
            afile.HasFieldsEnclosedInQuotes = True
            'String to capture all menu items that failed to parse.
            Dim errorlog As String
            Do While Not afile.EndOfData
                Try
                    currentrecord = afile.ReadFields
                    'Skip the header row.
                    If Not currentrecord(0) = "Item" Then
                        Dim menuitem As String = returnmenubyname(currentrecord(0))
                        Dim quantity As Integer = currentrecord(1)
                        Dim returnedarraylist As ArrayList = Returningredientsbymenu(menuitem)
                        If returnedarraylist.Count = 0 Then
                            'if menu item has no associated ingredient items or doesn't exist in the database.
                            If errorlog = "" Then errorlog = currentrecord(0) Else errorlog += vbNewLine & currentrecord(0)
                        End If
                        For i = 0 To returnedarraylist.Count - 1
                            'subtract the associated ingredient quantity * quantity stated in the POS system.
                            SubtractIngredients(Strings.Split(returnedarraylist(i), "$")(0), CInt(Strings.Split(returnedarraylist(i), "$")(1)) * quantity)
                        Next
                    End If
                Catch ex As Exception
                    If errorlog = "" Then errorlog = currentrecord(0) Else errorlog += vbNewLine & currentrecord(0)
                End Try
            Loop
            'Notifies user that the system has finished parsing the CSV file.
            MsgBox("System has finished parsing the CSV file: " & dlg.FileName, MsgBoxStyle.OkOnly, "Notice Me Senpai")
            'Show the advanced error message window to provide alternatives for handling unparsed menu items(if any exists).
            If Not errorlog = "" Then error6(errorlog)
            'Refreshes the ingredients view.
            Updateingredients()
        End If
    End Sub
#End Region
#Region "Right Click Menu"
    Private Sub dg_ingredient_MouseDown(sender As Object, e As MouseEventArgs) Handles dg_ingredient.MouseDown
        'this function ensures that the ingredient is also selected on a right mouse click event.
        If e.Button = MouseButtons.Right Then
            Dim hti As DataGridView.HitTestInfo = dg_ingredient.HitTest(e.X, e.Y)
            dg_ingredient.ClearSelection()
            If Not hti.RowIndex = -1 Then
                dg_ingredient.Rows(hti.RowIndex).Selected = True
            End If
        End If
    End Sub

    Private Sub dg_menu_MouseDown(sender As Object, e As MouseEventArgs) Handles dg_menu.MouseDown
        'this function ensures that the menu item is also selected on a right mouse click event.
        If e.Button = MouseButtons.Right Then
            Dim hti As DataGridView.HitTestInfo = dg_menu.HitTest(e.X, e.Y)
            dg_menu.ClearSelection()
            If Not hti.RowIndex = -1 Then
                dg_menu.Rows(hti.RowIndex).Selected = True
            End If
        End If
    End Sub
#End Region
#Region "Search and Filter"
    'The code below handles searching and filtering settings.
    Private Sub t_search_TextChanged(sender As Object, e As EventArgs) Handles t_search.TextChanged
        'create a 300ms pause between user keystrokes and search event so we know user is done typing.
        tmr_usertype_ing.Stop()
        tmr_usertype_ing.Start()
    End Sub

    Private Sub tmr_usertype_Tick(sender As Object, e As EventArgs) Handles tmr_usertype_ing.Tick
        tmr_usertype_ing.Stop()
        Updateingredients()
    End Sub

    Private Sub t_search_menu_TextChanged(sender As Object, e As EventArgs) Handles t_search_menu.TextChanged
        'create a 300ms pause between user keystrokes and search event so we know user is done typing.
        tmr_usertype_menu.Stop()
        tmr_usertype_menu.Start()
    End Sub

    Private Sub tmr_usertype_menu_Tick(sender As Object, e As EventArgs) Handles tmr_usertype_menu.Tick
        tmr_usertype_menu.Stop()
        UpdateMenuItems()
    End Sub

    Private Sub ts_flagged_Click(sender As Object, e As EventArgs) Handles ts_flagged.Click
        'system will only display flagged items when this item is checked.
        Updateingredients()
    End Sub
#End Region
End Class
