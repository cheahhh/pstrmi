﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PSTRMI")>
<Assembly: AssemblyDescription("PSTRMI Inventory Management System")>
<Assembly: AssemblyCompany("JLI Systems")>
<Assembly: AssemblyProduct("PSTRMI")>
<Assembly: AssemblyCopyright("Copyright © JLI Systems 2019")>
<Assembly: AssemblyTrademark("PSTRMI™")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f15e5dfd-9d15-4794-8633-e2003b4fae84")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>
