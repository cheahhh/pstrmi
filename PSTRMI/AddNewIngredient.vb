﻿Public Class AddNewIngredient
    Public editmode As Boolean = False
    Public itemid As Integer
    Dim itemexists As Boolean = False
    Private Sub bt_inc_Click(sender As Object, e As EventArgs) Handles bt_inc.Click
        'add to the quantity based on the units or containers specified.
        If Not (IsNumeric(l_value.Text) = False Or String.IsNullOrWhiteSpace(l_value.Text) = True Or String.IsNullOrWhiteSpace(cb_type.Text) = True) Then
            If Not l_value.Text < 0 Then
                If cb_type.SelectedIndex = 0 Then
                    If Not t_units.Value + CInt(l_value.Text) > t_units.Maximum Then t_units.Value += CInt(l_value.Text)
                Else
                    If Not t_units.Value + (CInt(l_value.Text) * t_unitspercontainer.Value) > t_units.Maximum Then t_units.Value += (CInt(l_value.Text) * t_unitspercontainer.Value)
                End If
            Else
                MsgBox("Calculation error, please ensure that the value and units per container is numeric.", MsgBoxStyle.OkOnly, "Error Code: 2")
            End If
        Else
            MsgBox("Calculation error, please ensure that the value and units per container is numeric.", MsgBoxStyle.OkOnly, "Error Code: 2")
        End If
    End Sub
    Private Sub TextBox_LostFocus(sender As Object, e As EventArgs) Handles t_name.LostFocus
        'check if there is conflict for the ingredient name.
        If Not Disposing = True Then
            pb_exists.Visible = True
            If editmode = False Then
                If GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name = " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString = "0" Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The ingredient name is available.")
                    itemexists = False
                    If CInt(GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString) > 0 Then
                        pb_exists.Image = My.Resources.attention_icon
                        Dim similaring As String
                        Dim dt As DataTable = GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True))
                        For i = 0 To dt.Rows.Count - 1
                            If similaring = "" Then similaring = dt.Rows(i).Item(0) & vbNewLine Else similaring += dt.Rows(i).Item(0) & vbNewLine
                        Next
                        ToolTip1.SetToolTip(pb_exists, "Similar ingredient exists:" & vbNewLine & similaring)
                        itemexists = False
                    End If
                Else
                    pb_exists.Image = My.Resources.close_delete_icon
                    ToolTip1.SetToolTip(pb_exists, "The ingredient name is NOT available.")
                    itemexists = True
                End If
            Else
                If GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name = " & Cnull(t_name.Text, True)).Rows(0).Item(0).ToString = "0" Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The ingredient name is available.")
                    itemexists = False
                    If CInt(GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True) & " AND NAME <> " & Cnull(GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(itemid)).Rows(0).Item(0).ToString, True)).Rows(0).Item(0).ToString) > 0 Then
                        pb_exists.Image = My.Resources.attention_icon
                        Dim similaring As String
                        Dim dt As DataTable = GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name LIKE " & Cnull("%" & t_name.Text & "%", True) & " AND Name <> " & Cnull(t_name.Text, True) & " AND NAME <> " & Cnull(GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(itemid)).Rows(0).Item(0).ToString, True))
                        For i = 0 To dt.Rows.Count - 1
                            If similaring = "" Then similaring = dt.Rows(i).Item(0) & vbNewLine Else similaring += dt.Rows(i).Item(0) & vbNewLine
                        Next
                        ToolTip1.SetToolTip(pb_exists, "Similar ingredient exists:" & vbNewLine & similaring)
                        itemexists = False
                    End If
                ElseIf GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(itemid)).Rows(0).Item(0).ToString = t_name.Text Then
                    pb_exists.Image = My.Resources.check_icon
                    ToolTip1.SetToolTip(pb_exists, "The ingredient name is available.")
                    itemexists = False
                Else
                    pb_exists.Image = My.Resources.close_delete_icon
                    ToolTip1.SetToolTip(pb_exists, "The ingredient name is NOT available.")
                    itemexists = True
                End If
            End If
        End If
    End Sub
    Private Sub TextBox_gotFocus(sender As Object, e As EventArgs) Handles t_name.GotFocus
        pb_exists.Visible = False
    End Sub
    Private Sub bt_dec_Click(sender As Object, e As EventArgs) Handles bt_dec.Click
        'subtract units by the units or containers specified.
        If Not (IsNumeric(l_value.Text) = False Or String.IsNullOrWhiteSpace(l_value.Text) = True Or String.IsNullOrWhiteSpace(cb_type.Text) = True) Then
            If Not l_value.Text < 0 Then
                If cb_type.SelectedIndex = 0 Then
                    If Not t_units.Value - CInt(l_value.Text) < t_units.Minimum Then t_units.Value -= CInt(l_value.Text)
                Else
                    If Not t_units.Value - (CInt(l_value.Text) * t_unitspercontainer.Value) < t_units.Minimum Then t_units.Value -= (CInt(l_value.Text) * t_unitspercontainer.Value)
                End If
            Else
                MsgBox("Calculation error, please ensure that the value and units per container is numeric.", MsgBoxStyle.OkOnly, "Error Code: 2")
            End If
        Else
            MsgBox("Calculation error, please ensure that the value and units per container is numeric.", MsgBoxStyle.OkOnly, "Error Code: 2")
        End If
    End Sub
    Public Sub Populatefields()
        'load the attributes for the ingredient.
        Dim dr As DataRow = GetData("SELECT * FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(itemid)).Rows(0)
        t_name.Text = dr.Item(1)
        t_units.Value = dr.Item(2)
        t_unitspercontainer.Value = dr.Item(3)
        t_low.Value = dr.Item(4)
    End Sub
    Private Sub MaterialRaisedButton3_Click(sender As Object, e As EventArgs) Handles btn_save.Click
        If Not (String.IsNullOrWhiteSpace(t_name.Text) = True Or IsNumeric(t_units.Value) = False Or IsNumeric(t_low.Value) = False Or IsNumeric(t_unitspercontainer.Value) = False Or itemexists = True) Then
            If editmode = False Then
                'add ingredient to database if no constraints are violated
                GetData("INSERT INTO [" & initcat & "].[dbo].[Ingredients] VALUES(" & Cnull(t_name.Text) & ", " & Cnull(t_units.Value) & ", " & Cnull(t_unitspercontainer.Value) & ", " & Cnull(t_low.Value) & ")")
            Else
                'update attributes for the ingredient if it is in edit mode.
                GetData("UPDATE [" & initcat & "].[dbo].[Ingredients] SET Name = " & Cnull(t_name.Text) & ", Units = " & Cnull(t_units.Value) & ", UnitsPerContainer = " & Cnull(t_unitspercontainer.Value) & ", OrderTrigger = " & Cnull(t_low.Value) & "WHERE IngredientID = " & Cnull(itemid))
            End If
            Me.Close()
            Form1.Show()
            Form1.Updateingredients()
        Else
            'build the error message based on the constraints violations.
            Dim errormessage As String
            If String.IsNullOrWhiteSpace(t_name.Text) = True Then errormessage += "Ingredient name cannot be blank." & vbNewLine
            If IsNumeric(t_low.Value) = False Then errormessage += "Ingredient threshold must be numeric." & vbNewLine
            If IsNumeric(t_unitspercontainer.Value) = False Then errormessage += "Units per container must be numeric." & vbNewLine
            If itemexists = True Then errormessage += "Ingredient name already exists in the database."
            MsgBox(errormessage, MsgBoxStyle.OkOnly, "Error Code: 1")
        End If
    End Sub
End Class