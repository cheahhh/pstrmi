﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login))
        Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
        Me.t_ip = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.t_user = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialLabel2 = New MaterialSkin.Controls.MaterialLabel()
        Me.t_pass = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialLabel3 = New MaterialSkin.Controls.MaterialLabel()
        Me.cb = New System.Windows.Forms.ComboBox()
        Me.MaterialLabel4 = New MaterialSkin.Controls.MaterialLabel()
        Me.cb_remember = New MaterialSkin.Controls.MaterialCheckBox()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.SuspendLayout()
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.AutoSize = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel1.Location = New System.Drawing.Point(12, 9)
        Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(72, 19)
        Me.MaterialLabel1.TabIndex = 0
        Me.MaterialLabel1.Text = "Server IP:"
        '
        't_ip
        '
        Me.t_ip.Depth = 0
        Me.t_ip.Hint = ""
        Me.t_ip.Location = New System.Drawing.Point(97, 9)
        Me.t_ip.MaxLength = 32767
        Me.t_ip.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_ip.Name = "t_ip"
        Me.t_ip.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_ip.SelectedText = ""
        Me.t_ip.SelectionLength = 0
        Me.t_ip.SelectionStart = 0
        Me.t_ip.Size = New System.Drawing.Size(236, 23)
        Me.t_ip.TabIndex = 1
        Me.t_ip.TabStop = False
        Me.t_ip.UseSystemPasswordChar = False
        '
        't_user
        '
        Me.t_user.Depth = 0
        Me.t_user.Hint = ""
        Me.t_user.Location = New System.Drawing.Point(97, 38)
        Me.t_user.MaxLength = 32767
        Me.t_user.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_user.Name = "t_user"
        Me.t_user.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_user.SelectedText = ""
        Me.t_user.SelectionLength = 0
        Me.t_user.SelectionStart = 0
        Me.t_user.Size = New System.Drawing.Size(236, 23)
        Me.t_user.TabIndex = 2
        Me.t_user.TabStop = False
        Me.t_user.UseSystemPasswordChar = False
        '
        'MaterialLabel2
        '
        Me.MaterialLabel2.AutoSize = True
        Me.MaterialLabel2.Depth = 0
        Me.MaterialLabel2.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel2.Location = New System.Drawing.Point(12, 38)
        Me.MaterialLabel2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel2.Name = "MaterialLabel2"
        Me.MaterialLabel2.Size = New System.Drawing.Size(44, 19)
        Me.MaterialLabel2.TabIndex = 2
        Me.MaterialLabel2.Text = "User:"
        '
        't_pass
        '
        Me.t_pass.Depth = 0
        Me.t_pass.Hint = ""
        Me.t_pass.Location = New System.Drawing.Point(97, 67)
        Me.t_pass.MaxLength = 32767
        Me.t_pass.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_pass.Name = "t_pass"
        Me.t_pass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.t_pass.SelectedText = ""
        Me.t_pass.SelectionLength = 0
        Me.t_pass.SelectionStart = 0
        Me.t_pass.Size = New System.Drawing.Size(236, 23)
        Me.t_pass.TabIndex = 3
        Me.t_pass.TabStop = False
        Me.t_pass.UseSystemPasswordChar = False
        '
        'MaterialLabel3
        '
        Me.MaterialLabel3.AutoSize = True
        Me.MaterialLabel3.Depth = 0
        Me.MaterialLabel3.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel3.Location = New System.Drawing.Point(12, 67)
        Me.MaterialLabel3.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel3.Name = "MaterialLabel3"
        Me.MaterialLabel3.Size = New System.Drawing.Size(79, 19)
        Me.MaterialLabel3.TabIndex = 4
        Me.MaterialLabel3.Text = "Password:"
        '
        'cb
        '
        Me.cb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb.FormattingEnabled = True
        Me.cb.Location = New System.Drawing.Point(97, 97)
        Me.cb.Name = "cb"
        Me.cb.Size = New System.Drawing.Size(235, 21)
        Me.cb.TabIndex = 4
        '
        'MaterialLabel4
        '
        Me.MaterialLabel4.AutoSize = True
        Me.MaterialLabel4.Depth = 0
        Me.MaterialLabel4.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel4.Location = New System.Drawing.Point(12, 99)
        Me.MaterialLabel4.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel4.Name = "MaterialLabel4"
        Me.MaterialLabel4.Size = New System.Drawing.Size(76, 19)
        Me.MaterialLabel4.TabIndex = 7
        Me.MaterialLabel4.Text = "Database:"
        '
        'cb_remember
        '
        Me.cb_remember.AutoSize = True
        Me.cb_remember.Depth = 0
        Me.cb_remember.Font = New System.Drawing.Font("Roboto", 10.0!)
        Me.cb_remember.Location = New System.Drawing.Point(9, 136)
        Me.cb_remember.Margin = New System.Windows.Forms.Padding(0)
        Me.cb_remember.MouseLocation = New System.Drawing.Point(-1, -1)
        Me.cb_remember.MouseState = MaterialSkin.MouseState.HOVER
        Me.cb_remember.Name = "cb_remember"
        Me.cb_remember.Ripple = True
        Me.cb_remember.Size = New System.Drawing.Size(171, 30)
        Me.cb_remember.TabIndex = 5
        Me.cb_remember.Text = "Remember Credentials"
        Me.cb_remember.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(257, 139)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(75, 23)
        Me.MaterialRaisedButton1.TabIndex = 6
        Me.MaterialRaisedButton1.Text = "Login"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(344, 175)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Controls.Add(Me.cb_remember)
        Me.Controls.Add(Me.MaterialLabel4)
        Me.Controls.Add(Me.cb)
        Me.Controls.Add(Me.t_pass)
        Me.Controls.Add(Me.MaterialLabel3)
        Me.Controls.Add(Me.t_user)
        Me.Controls.Add(Me.MaterialLabel2)
        Me.Controls.Add(Me.t_ip)
        Me.Controls.Add(Me.MaterialLabel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_ip As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents t_user As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialLabel2 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_pass As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialLabel3 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents cb As ComboBox
    Friend WithEvents MaterialLabel4 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents cb_remember As MaterialSkin.Controls.MaterialCheckBox
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
End Class
