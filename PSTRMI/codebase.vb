﻿Imports System.Data.SqlClient

Module codebase
    Private myConn As SqlConnection
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader
    'Credentials for SQL Backend
    Public username As String = ""
    Public password As String = ""
    'Server string format is IP\ServerName
    Public serverstring As String = ""
    'Initial Catalog
    Public initcat As String = ""
    Public Function Cnull(ByVal concatstring As String, ByVal Optional isdate As Boolean = False)
        'This is the check null function, if the input string we will fill in with a DBNull, else we enclose the string with single quotes if it's a string(non-numeric) or when it's a date.
        'To-Do: check if it sanitizes inputs to prevent SQL Injections.
        If concatstring = "" Then
            Return "null"
        Else
            If IsNumeric(concatstring) = False Then
                Return Chr(39) & concatstring.Replace(Chr(39), Chr(39) & "+Char(39)+" & Chr(39)) & Chr(39)
            Else
                If isdate = False Then
                    Return concatstring
                Else
                    Return Chr(39) & concatstring & Chr(39)
                End If
            End If
        End If
    End Function
    Public Function GetData(ByVal sqlCommand As String, ByVal Optional useinitcat As Boolean = True) As DataTable
        'This function passes SQL queries onto the server.
        'MsgBox(sqlCommand)
        Dim sqlConnectionString As String
        If useinitcat = True Then
            sqlConnectionString = "Data Source=" & serverstring & ";Initial Catalog=" & initcat & ";User ID=" & username & ";Password=" & password
        Else
            sqlConnectionString = "Data Source=" & serverstring & ";User ID=" & username & ";Password=" & password
        End If
        myConn = New SqlConnection()
        myConn.ConnectionString = sqlConnectionString
        Dim command As New SqlCommand(sqlCommand, myConn)
        Dim adapter As SqlDataAdapter = New SqlDataAdapter()
        adapter.SelectCommand = command
        Dim table As New DataTable
        table.Locale = System.Globalization.CultureInfo.InvariantCulture
        adapter.Fill(table)
        Return table
    End Function
    Public Function Returningredientbyname(ByVal ingname As String) As String
        'this function returns ingredient id based on the name.
        If Not GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name = " & Cnull(ingname)).Rows(0).Item(0) = 0 Then
            Return GetData("SELECT IngredientID FROM [" & initcat & "].[dbo].[Ingredients] WHERE Name = " & Cnull(ingname)).Rows(0).Item(0)
        End If
    End Function
    Public Function returningredientbyid(ByVal id As Integer) As String
        'this function returns the ingredient name from the id.
        If Not GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(id)).Rows(0).Item(0) = 0 Then
            Return GetData("SELECT Name FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(id)).Rows(0).Item(0)
        End If
    End Function
    Public Function returnmenubyname(ByVal menuitem As String) As Integer
        'this function returns the menu id from the menu item name.
        If Not GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name = " & Cnull(menuitem)).Rows(0).Item(0) = 0 Then
            Return GetData("SELECT MenuID FROM [" & initcat & "].[dbo].[MenuItems] WHERE Name = " & Cnull(menuitem)).Rows(0).Item(0)
        End If
    End Function
    Public Function Returningredientsbymenu(ByVal menuid As Integer) As ArrayList
        'this function returns a list of associated ingredients by a menu id.
        Dim ingredientlist As New ArrayList
        If Not GetData("SELECT COUNT(*) FROM [" & initcat & "].[dbo].[BridgeTable] WHERE MenuID = " & Cnull(menuid)).Rows(0).Item(0) = 0 Then
            Dim ingredienttable As DataTable = GetData("SELECT CONCAT(IngredientID, '$', Quantity) FROM [" & initcat & "].[dbo].[BridgeTable] WHERE MenuID = " & Cnull(menuid))
            For i = 0 To ingredienttable.Rows.Count - 1
                ingredientlist.Add(ingredienttable.Rows(i).Item(0))
            Next
        End If
        Return ingredientlist
    End Function
    Public Sub SubtractIngredients(ByVal ingredientid As Integer, ByVal quantity As Integer)
        'this function subtracts the ingredient quantity by the provided quantity.
        Dim currentquantity As Integer = GetData("SELECT Units FROM [" & initcat & "].[dbo].[Ingredients] WHERE IngredientID = " & Cnull(ingredientid)).Rows(0).Item(0)
        Dim newquantity As Integer = currentquantity - quantity
        If Not newquantity < 0 Then
            GetData("UPDATE [" & initcat & "].[dbo].[Ingredients] SET Units = " & newquantity & " WHERE IngredientID = " & Cnull(ingredientid))
        Else
            MsgBox("Unable to subtract Ingredient " & returningredientbyid(ingredientid) & " as the quantity will result in a negative value.", MsgBoxStyle.OkOnly, "Error Code: 5")
        End If
    End Sub
    Public Sub error6(ByVal errors As String)
        'this function shows the advanced error message which provides options for unparsed menu items.
        Dim dlg As New frm_csverror
        dlg.t_errors.Text = errors
        dlg.ShowDialog()
    End Sub
End Module
