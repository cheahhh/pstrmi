﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_csverror
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.t_errors = New System.Windows.Forms.TextBox()
        Me.btn_copy = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.btn_add = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.btn_close = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.tt_error = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(268, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "The following menu items were not parsed successfully:"
        '
        't_errors
        '
        Me.t_errors.Location = New System.Drawing.Point(15, 25)
        Me.t_errors.Multiline = True
        Me.t_errors.Name = "t_errors"
        Me.t_errors.ReadOnly = True
        Me.t_errors.Size = New System.Drawing.Size(281, 216)
        Me.t_errors.TabIndex = 1
        '
        'btn_copy
        '
        Me.btn_copy.Depth = 0
        Me.btn_copy.Location = New System.Drawing.Point(15, 247)
        Me.btn_copy.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_copy.Name = "btn_copy"
        Me.btn_copy.Primary = True
        Me.btn_copy.Size = New System.Drawing.Size(55, 31)
        Me.btn_copy.TabIndex = 2
        Me.btn_copy.Text = "copy"
        Me.tt_error.SetToolTip(Me.btn_copy, "Copy all the items to the clipboard")
        Me.btn_copy.UseVisualStyleBackColor = True
        '
        'btn_add
        '
        Me.btn_add.Depth = 0
        Me.btn_add.Location = New System.Drawing.Point(76, 247)
        Me.btn_add.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_add.Name = "btn_add"
        Me.btn_add.Primary = True
        Me.btn_add.Size = New System.Drawing.Size(55, 31)
        Me.btn_add.TabIndex = 3
        Me.btn_add.Text = "add"
        Me.tt_error.SetToolTip(Me.btn_add, "Add all the items into the database")
        Me.btn_add.UseVisualStyleBackColor = True
        '
        'btn_close
        '
        Me.btn_close.Depth = 0
        Me.btn_close.Location = New System.Drawing.Point(241, 247)
        Me.btn_close.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_close.Name = "btn_close"
        Me.btn_close.Primary = True
        Me.btn_close.Size = New System.Drawing.Size(55, 31)
        Me.btn_close.TabIndex = 4
        Me.btn_close.Text = "close"
        Me.tt_error.SetToolTip(Me.btn_close, "Close the window")
        Me.btn_close.UseVisualStyleBackColor = True
        '
        'frm_csverror
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(308, 290)
        Me.Controls.Add(Me.btn_close)
        Me.Controls.Add(Me.btn_add)
        Me.Controls.Add(Me.btn_copy)
        Me.Controls.Add(Me.t_errors)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm_csverror"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Error Code: 6"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents t_errors As TextBox
    Friend WithEvents btn_copy As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents tt_error As ToolTip
    Friend WithEvents btn_add As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents btn_close As MaterialSkin.Controls.MaterialRaisedButton
End Class
