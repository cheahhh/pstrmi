﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddMenuItem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddMenuItem))
        Me.t_name = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_save = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.dg_ingredient = New System.Windows.Forms.DataGridView()
        Me.IngredientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Quantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cm_menuedit = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cb_ingredient = New System.Windows.Forms.ComboBox()
        Me.bt_inc = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.l_incdec = New MaterialSkin.Controls.MaterialLabel()
        Me.t_units = New System.Windows.Forms.NumericUpDown()
        Me.MaterialLabel2 = New MaterialSkin.Controls.MaterialLabel()
        Me.pb_exists = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        CType(Me.dg_ingredient, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cm_menuedit.SuspendLayout()
        CType(Me.t_units, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_exists, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        't_name
        '
        Me.t_name.Depth = 0
        Me.t_name.Hint = "Insert Menu Item Name"
        Me.t_name.Location = New System.Drawing.Point(112, 9)
        Me.t_name.MaxLength = 50
        Me.t_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_name.Name = "t_name"
        Me.t_name.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_name.SelectedText = ""
        Me.t_name.SelectionLength = 0
        Me.t_name.SelectionStart = 0
        Me.t_name.Size = New System.Drawing.Size(215, 23)
        Me.t_name.TabIndex = 1
        Me.t_name.TabStop = False
        Me.t_name.UseSystemPasswordChar = False
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.AutoSize = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel1.Location = New System.Drawing.Point(12, 9)
        Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(94, 19)
        Me.MaterialLabel1.TabIndex = 2
        Me.MaterialLabel1.Text = "Menu Name:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btn_save)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 234)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(622, 42)
        Me.Panel1.TabIndex = 4
        '
        'btn_save
        '
        Me.btn_save.Depth = 0
        Me.btn_save.Location = New System.Drawing.Point(551, 7)
        Me.btn_save.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Primary = True
        Me.btn_save.Size = New System.Drawing.Size(59, 23)
        Me.btn_save.TabIndex = 6
        Me.btn_save.Text = "Save"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'dg_ingredient
        '
        Me.dg_ingredient.AllowUserToAddRows = False
        Me.dg_ingredient.AllowUserToDeleteRows = False
        Me.dg_ingredient.BackgroundColor = System.Drawing.Color.White
        Me.dg_ingredient.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg_ingredient.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IngredientName, Me.Quantity})
        Me.dg_ingredient.ContextMenuStrip = Me.cm_menuedit
        Me.dg_ingredient.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dg_ingredient.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.dg_ingredient.Location = New System.Drawing.Point(0, 108)
        Me.dg_ingredient.MultiSelect = False
        Me.dg_ingredient.Name = "dg_ingredient"
        Me.dg_ingredient.ReadOnly = True
        Me.dg_ingredient.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dg_ingredient.Size = New System.Drawing.Size(622, 126)
        Me.dg_ingredient.TabIndex = 5
        '
        'IngredientName
        '
        Me.IngredientName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.IngredientName.HeaderText = "Ingredient"
        Me.IngredientName.Name = "IngredientName"
        Me.IngredientName.ReadOnly = True
        '
        'Quantity
        '
        Me.Quantity.HeaderText = "Quantity"
        Me.Quantity.Name = "Quantity"
        Me.Quantity.ReadOnly = True
        '
        'cm_menuedit
        '
        Me.cm_menuedit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.cm_menuedit.Name = "cm_menuedit"
        Me.cm_menuedit.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'cb_ingredient
        '
        Me.cb_ingredient.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cb_ingredient.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cb_ingredient.FormattingEnabled = True
        Me.cb_ingredient.Location = New System.Drawing.Point(97, 54)
        Me.cb_ingredient.Name = "cb_ingredient"
        Me.cb_ingredient.Size = New System.Drawing.Size(308, 21)
        Me.cb_ingredient.TabIndex = 2
        '
        'bt_inc
        '
        Me.bt_inc.Depth = 0
        Me.bt_inc.Location = New System.Drawing.Point(540, 52)
        Me.bt_inc.MouseState = MaterialSkin.MouseState.HOVER
        Me.bt_inc.Name = "bt_inc"
        Me.bt_inc.Primary = True
        Me.bt_inc.Size = New System.Drawing.Size(70, 23)
        Me.bt_inc.TabIndex = 4
        Me.bt_inc.Text = "+"
        Me.bt_inc.UseVisualStyleBackColor = True
        '
        'l_incdec
        '
        Me.l_incdec.AutoSize = True
        Me.l_incdec.Depth = 0
        Me.l_incdec.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.l_incdec.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.l_incdec.Location = New System.Drawing.Point(12, 53)
        Me.l_incdec.MouseState = MaterialSkin.MouseState.HOVER
        Me.l_incdec.Name = "l_incdec"
        Me.l_incdec.Size = New System.Drawing.Size(79, 19)
        Me.l_incdec.TabIndex = 12
        Me.l_incdec.Text = "Ingredient:"
        '
        't_units
        '
        Me.t_units.Location = New System.Drawing.Point(465, 53)
        Me.t_units.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.t_units.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.t_units.Name = "t_units"
        Me.t_units.Size = New System.Drawing.Size(69, 20)
        Me.t_units.TabIndex = 3
        Me.t_units.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'MaterialLabel2
        '
        Me.MaterialLabel2.AutoSize = True
        Me.MaterialLabel2.Depth = 0
        Me.MaterialLabel2.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel2.Location = New System.Drawing.Point(411, 53)
        Me.MaterialLabel2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel2.Name = "MaterialLabel2"
        Me.MaterialLabel2.Size = New System.Drawing.Size(48, 19)
        Me.MaterialLabel2.TabIndex = 16
        Me.MaterialLabel2.Text = "Units:"
        '
        'pb_exists
        '
        Me.pb_exists.Image = Global.PSTRMI.My.Resources.Resources.close_delete_icon
        Me.pb_exists.Location = New System.Drawing.Point(333, 8)
        Me.pb_exists.Name = "pb_exists"
        Me.pb_exists.Size = New System.Drawing.Size(24, 24)
        Me.pb_exists.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb_exists.TabIndex = 18
        Me.pb_exists.TabStop = False
        '
        'AddMenuItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(622, 276)
        Me.Controls.Add(Me.pb_exists)
        Me.Controls.Add(Me.t_units)
        Me.Controls.Add(Me.MaterialLabel2)
        Me.Controls.Add(Me.cb_ingredient)
        Me.Controls.Add(Me.bt_inc)
        Me.Controls.Add(Me.l_incdec)
        Me.Controls.Add(Me.dg_ingredient)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.t_name)
        Me.Controls.Add(Me.MaterialLabel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddMenuItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Menu Item"
        Me.Panel1.ResumeLayout(False)
        CType(Me.dg_ingredient, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cm_menuedit.ResumeLayout(False)
        CType(Me.t_units, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_exists, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents t_name As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents dg_ingredient As DataGridView
    Friend WithEvents IngredientName As DataGridViewTextBoxColumn
    Friend WithEvents Quantity As DataGridViewTextBoxColumn
    Friend WithEvents btn_save As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents cb_ingredient As ComboBox
    Friend WithEvents bt_inc As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents l_incdec As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_units As NumericUpDown
    Friend WithEvents MaterialLabel2 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents pb_exists As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents cm_menuedit As ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As ToolStripMenuItem
End Class
