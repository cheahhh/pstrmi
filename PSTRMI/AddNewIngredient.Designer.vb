﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddNewIngredient
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddNewIngredient))
        Me.MaterialLabel1 = New MaterialSkin.Controls.MaterialLabel()
        Me.t_name = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.MaterialLabel2 = New MaterialSkin.Controls.MaterialLabel()
        Me.t_units = New System.Windows.Forms.NumericUpDown()
        Me.t_unitspercontainer = New System.Windows.Forms.NumericUpDown()
        Me.MaterialLabel3 = New MaterialSkin.Controls.MaterialLabel()
        Me.t_low = New System.Windows.Forms.NumericUpDown()
        Me.MaterialLabel4 = New MaterialSkin.Controls.MaterialLabel()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.l_incdec = New MaterialSkin.Controls.MaterialLabel()
        Me.l_value = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.bt_inc = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.cb_type = New System.Windows.Forms.ComboBox()
        Me.bt_dec = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.btn_save = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.pb_exists = New System.Windows.Forms.PictureBox()
        CType(Me.t_units, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.t_unitspercontainer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.t_low, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pb_exists, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.AutoSize = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel1.Location = New System.Drawing.Point(12, 9)
        Me.MaterialLabel1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(123, 19)
        Me.MaterialLabel1.TabIndex = 0
        Me.MaterialLabel1.Text = "Ingredient Name:"
        '
        't_name
        '
        Me.t_name.Depth = 0
        Me.t_name.Hint = "Insert Ingredient Name"
        Me.t_name.Location = New System.Drawing.Point(141, 9)
        Me.t_name.MaxLength = 50
        Me.t_name.MouseState = MaterialSkin.MouseState.HOVER
        Me.t_name.Name = "t_name"
        Me.t_name.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.t_name.SelectedText = ""
        Me.t_name.SelectionLength = 0
        Me.t_name.SelectionStart = 0
        Me.t_name.Size = New System.Drawing.Size(215, 23)
        Me.t_name.TabIndex = 1
        Me.t_name.TabStop = False
        Me.t_name.UseSystemPasswordChar = False
        '
        'MaterialLabel2
        '
        Me.MaterialLabel2.AutoSize = True
        Me.MaterialLabel2.Depth = 0
        Me.MaterialLabel2.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel2.Location = New System.Drawing.Point(12, 50)
        Me.MaterialLabel2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel2.Name = "MaterialLabel2"
        Me.MaterialLabel2.Size = New System.Drawing.Size(48, 19)
        Me.MaterialLabel2.TabIndex = 2
        Me.MaterialLabel2.Text = "Units:"
        '
        't_units
        '
        Me.t_units.Location = New System.Drawing.Point(66, 50)
        Me.t_units.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.t_units.Name = "t_units"
        Me.t_units.Size = New System.Drawing.Size(69, 20)
        Me.t_units.TabIndex = 2
        '
        't_unitspercontainer
        '
        Me.t_unitspercontainer.Location = New System.Drawing.Point(290, 50)
        Me.t_unitspercontainer.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.t_unitspercontainer.Name = "t_unitspercontainer"
        Me.t_unitspercontainer.Size = New System.Drawing.Size(69, 20)
        Me.t_unitspercontainer.TabIndex = 3
        '
        'MaterialLabel3
        '
        Me.MaterialLabel3.AutoSize = True
        Me.MaterialLabel3.Depth = 0
        Me.MaterialLabel3.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel3.Location = New System.Drawing.Point(141, 50)
        Me.MaterialLabel3.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel3.Name = "MaterialLabel3"
        Me.MaterialLabel3.Size = New System.Drawing.Size(143, 19)
        Me.MaterialLabel3.TabIndex = 4
        Me.MaterialLabel3.Text = "Units Per Container:"
        '
        't_low
        '
        Me.t_low.Location = New System.Drawing.Point(509, 50)
        Me.t_low.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.t_low.Name = "t_low"
        Me.t_low.Size = New System.Drawing.Size(69, 20)
        Me.t_low.TabIndex = 4
        '
        'MaterialLabel4
        '
        Me.MaterialLabel4.AutoSize = True
        Me.MaterialLabel4.Depth = 0
        Me.MaterialLabel4.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.MaterialLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialLabel4.Location = New System.Drawing.Point(365, 50)
        Me.MaterialLabel4.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialLabel4.Name = "MaterialLabel4"
        Me.MaterialLabel4.Size = New System.Drawing.Size(138, 19)
        Me.MaterialLabel4.TabIndex = 6
        Me.MaterialLabel4.Text = "Set Low Threshold:"
        Me.ToolTip1.SetToolTip(Me.MaterialLabel4, "If the quantity of the ingredient dips below this amount then the ingredient will" &
        " be flagged in red.")
        '
        'l_incdec
        '
        Me.l_incdec.AutoSize = True
        Me.l_incdec.Depth = 0
        Me.l_incdec.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.l_incdec.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.l_incdec.Location = New System.Drawing.Point(12, 95)
        Me.l_incdec.MouseState = MaterialSkin.MouseState.HOVER
        Me.l_incdec.Name = "l_incdec"
        Me.l_incdec.Size = New System.Drawing.Size(198, 19)
        Me.l_incdec.TabIndex = 8
        Me.l_incdec.Text = "Increase/Decrease Units By:"
        '
        'l_value
        '
        Me.l_value.Depth = 0
        Me.l_value.Hint = "Insert Value"
        Me.l_value.Location = New System.Drawing.Point(219, 95)
        Me.l_value.MaxLength = 32767
        Me.l_value.MouseState = MaterialSkin.MouseState.HOVER
        Me.l_value.Name = "l_value"
        Me.l_value.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.l_value.SelectedText = ""
        Me.l_value.SelectionLength = 0
        Me.l_value.SelectionStart = 0
        Me.l_value.Size = New System.Drawing.Size(100, 23)
        Me.l_value.TabIndex = 5
        Me.l_value.TabStop = False
        Me.l_value.UseSystemPasswordChar = False
        '
        'bt_inc
        '
        Me.bt_inc.Depth = 0
        Me.bt_inc.Location = New System.Drawing.Point(411, 97)
        Me.bt_inc.MouseState = MaterialSkin.MouseState.HOVER
        Me.bt_inc.Name = "bt_inc"
        Me.bt_inc.Primary = True
        Me.bt_inc.Size = New System.Drawing.Size(37, 23)
        Me.bt_inc.TabIndex = 7
        Me.bt_inc.Text = "+"
        Me.bt_inc.UseVisualStyleBackColor = True
        '
        'cb_type
        '
        Me.cb_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_type.FormattingEnabled = True
        Me.cb_type.Items.AddRange(New Object() {"Unit", "Container"})
        Me.cb_type.Location = New System.Drawing.Point(325, 97)
        Me.cb_type.Name = "cb_type"
        Me.cb_type.Size = New System.Drawing.Size(80, 21)
        Me.cb_type.TabIndex = 6
        '
        'bt_dec
        '
        Me.bt_dec.Depth = 0
        Me.bt_dec.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bt_dec.Location = New System.Drawing.Point(454, 97)
        Me.bt_dec.MouseState = MaterialSkin.MouseState.HOVER
        Me.bt_dec.Name = "bt_dec"
        Me.bt_dec.Primary = True
        Me.bt_dec.Size = New System.Drawing.Size(37, 23)
        Me.bt_dec.TabIndex = 8
        Me.bt_dec.Text = "-"
        Me.bt_dec.UseVisualStyleBackColor = True
        '
        'btn_save
        '
        Me.btn_save.Depth = 0
        Me.btn_save.Location = New System.Drawing.Point(519, 97)
        Me.btn_save.MouseState = MaterialSkin.MouseState.HOVER
        Me.btn_save.Name = "btn_save"
        Me.btn_save.Primary = True
        Me.btn_save.Size = New System.Drawing.Size(59, 23)
        Me.btn_save.TabIndex = 9
        Me.btn_save.Text = "Save"
        Me.btn_save.UseVisualStyleBackColor = True
        '
        'pb_exists
        '
        Me.pb_exists.Image = Global.PSTRMI.My.Resources.Resources.close_delete_icon
        Me.pb_exists.Location = New System.Drawing.Point(363, 9)
        Me.pb_exists.Name = "pb_exists"
        Me.pb_exists.Size = New System.Drawing.Size(24, 24)
        Me.pb_exists.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb_exists.TabIndex = 14
        Me.pb_exists.TabStop = False
        '
        'AddNewIngredient
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(594, 142)
        Me.Controls.Add(Me.pb_exists)
        Me.Controls.Add(Me.btn_save)
        Me.Controls.Add(Me.bt_dec)
        Me.Controls.Add(Me.cb_type)
        Me.Controls.Add(Me.bt_inc)
        Me.Controls.Add(Me.l_value)
        Me.Controls.Add(Me.l_incdec)
        Me.Controls.Add(Me.t_low)
        Me.Controls.Add(Me.MaterialLabel4)
        Me.Controls.Add(Me.t_unitspercontainer)
        Me.Controls.Add(Me.MaterialLabel3)
        Me.Controls.Add(Me.t_units)
        Me.Controls.Add(Me.MaterialLabel2)
        Me.Controls.Add(Me.t_name)
        Me.Controls.Add(Me.MaterialLabel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddNewIngredient"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add New Ingredient"
        CType(Me.t_units, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.t_unitspercontainer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.t_low, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pb_exists, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MaterialLabel1 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_name As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents MaterialLabel2 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_units As NumericUpDown
    Friend WithEvents t_unitspercontainer As NumericUpDown
    Friend WithEvents MaterialLabel3 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents t_low As NumericUpDown
    Friend WithEvents MaterialLabel4 As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents l_incdec As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents l_value As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents bt_inc As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents cb_type As ComboBox
    Friend WithEvents bt_dec As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents btn_save As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents pb_exists As PictureBox
End Class
