﻿Public Class frm_csverror
    Private Sub btn_copy_Click(sender As Object, e As EventArgs) Handles btn_copy.Click
        'copy the data onto system clipboard.
        Clipboard.SetText(t_errors.Text)
        MsgBox("Copied to clipboard!")
    End Sub

    Private Sub btn_add_Click(sender As Object, e As EventArgs) Handles btn_add.Click
        'prompt the user to add all menu item one by one.
        'all menu items are required to have at least one associated ingredient.
        Dim allingredients As String() = t_errors.Text.Split(vbNewLine)
        For i = 0 To allingredients.Count - 1
            If Not allingredients(i) = "" Then
                Me.Hide()
                Dim dlg As New AddMenuItem
                dlg.t_name.Text = allingredients(i)
                dlg.ShowDialog()
            End If
        Next
        DialogResult = DialogResult.OK
    End Sub

    Private Sub btn_close_Click(sender As Object, e As EventArgs) Handles btn_close.Click
        DialogResult = DialogResult.OK
    End Sub
End Class